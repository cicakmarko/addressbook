//
//  ContactsViewSearchModelTest.m
//  AddressBook
//
//  Created by Marko Cicak on 4/10/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "Contact.h"
#import "ContactDao.h"
#import "ContactsViewSearchModel.h"

@interface ContactsViewSearchModelTest : XCTestCase
{
    ContactsViewSearchModel* model;
}
@end

@implementation ContactsViewSearchModelTest

- (void) setUp
{
    [super setUp];
    SREG.coredata = nil;
    [self prepareData];
    model = [[ContactsViewSearchModel alloc] init];
}

- (void) tearDown
{
    model = nil;
    [super tearDown];
}

- (void) testSearchResults
{
    model.query = @"in";
    XCTAssertEqual([model tableView:nil numberOfRowsInSection:0], 2);
    Contact* c = [model contactAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    XCTAssertEqualObjects(c.lastName, @"Martin");
    c = [model contactAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    XCTAssertEqualObjects(c.lastName, @"Starr");

    model.query = @"ing";
    XCTAssertEqual([model tableView:nil numberOfRowsInSection:0], 1);
    c = [model contactAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    XCTAssertEqualObjects(c.lastName, @"Starr");
}

- (void) prepareData
{
    ContactDao* contactDao = [[ContactDao alloc] init];
    Contact* c1 = [contactDao insertNew];
    c1.contactID = @"1";
    c1.firstName = @"George";
    c1.lastName = @"Harrison";

    Contact* c2 = [contactDao insertNew];
    c2.contactID = @"2";
    c2.firstName = @"John";
    c2.lastName = @"Lennon";

    Contact* c3 = [contactDao insertNew];
    c3.contactID = @"3";
    c3.firstName = @"George";
    c3.lastName = @"Martin";

    Contact* c4 = [contactDao insertNew];
    c4.contactID = @"4";
    c4.firstName = @"Paul";
    c4.lastName = @"McCartney";

    Contact* c5 = [contactDao insertNew];
    c5.contactID = @"5";
    c5.firstName = @"Ringo";
    c5.lastName = @"Starr";

    [contactDao save];
}

@end
