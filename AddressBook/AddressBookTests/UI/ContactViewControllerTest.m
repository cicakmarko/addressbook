//
//  ContactViewControllerTest.m
//  AddressBook
//
//  Created by Marko Cicak on 4/13/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <objc/runtime.h>
#import "ContactViewController.h"
#import "ContactDao.h"
#import "Contact.h"
#import "EmailDao.h"
#import "Email.h"
#import "ContactHeaderView.h"
#import "ContactViewModel.h"
#import "ContactViewModelMock.h"
#import "ContactAddressCell.h"
#import "NLBaseTests.h"

@interface ContactViewController ()
@property(nonatomic, strong) id <ContactViewModel> model;
@end


@interface ContactViewControllerTest : NLBaseTests
{
    ContactViewController* viewController;
}
@end

@implementation ContactViewControllerTest

- (void) setUp
{
    viewController = [[ContactViewController alloc] init];
    viewController.contactID = @"1";
    viewController.model = [[ContactViewModelMock alloc] init];
    [self prepareData];
    [super setUp];
}

- (void) tearDown
{
    viewController = nil;
    [super tearDown];
}

- (void) testControllerObservesNotification
{
    NLAssertNotObservingNotification(viewController, @"ContactRetrievedNotification", @"");
    [viewController viewWillAppear:NO];
    NLAssertObservingNotification(viewController, @"ContactRetrievedNotification", @"");
    [viewController viewWillDisappear:NO];
    NLAssertNotObservingNotification(viewController, @"ContactRetrievedNotification", @"");
}

- (void) testHeaderData
{
    ContactHeaderView* headerView = (ContactHeaderView*) viewController.tableView.tableHeaderView;
    XCTAssertEqualObjects(headerView.titleLabel.text, @"Erwin Schrödinger");
}

- (void) testTableSectionsCount
{
    XCTAssertEqual(viewController.tableView.numberOfSections, 3);
}

- (void) testPhoneSection
{
    XCTAssertEqualObjects([viewController tableView:nil titleForHeaderInSection:0], @"Phone");
    XCTAssertEqual([viewController tableView:nil numberOfRowsInSection:0], 1);
    NSIndexPath* path = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell* cell = [viewController tableView:nil cellForRowAtIndexPath:path];
    XCTAssertEqualObjects(cell.textLabel.text, @"+43016435221");
}

- (void) testAddressSection
{
    XCTAssertEqualObjects([viewController tableView:nil titleForHeaderInSection:1], @"Address");
    XCTAssertEqual([viewController tableView:nil numberOfRowsInSection:1], 1);
    NSIndexPath* path = [NSIndexPath indexPathForRow:0 inSection:1];
    ContactAddressCell* cell = (ContactAddressCell*) [viewController tableView:nil cellForRowAtIndexPath:path];
    XCTAssertEqualObjects(cell.streetLabel.text, @"14 Millergasse");
    XCTAssertEqualObjects(cell.cityLabel.text, @"1030 Vienna");
    XCTAssertEqualObjects(cell.stateLabel.text, @"Austria");
}

- (void) testEmailsSection
{
    XCTAssertEqualObjects([viewController tableView:nil titleForHeaderInSection:2], @"Emails");
    XCTAssertEqual([viewController tableView:nil numberOfRowsInSection:2], 3);
    NSIndexPath* path = [NSIndexPath indexPathForRow:0 inSection:2];
    UITableViewCell* cell = [viewController tableView:nil cellForRowAtIndexPath:path];
    XCTAssertEqualObjects(cell.textLabel.text, @"erwin.schrodinger@gmail.com");

    path = [NSIndexPath indexPathForRow:1 inSection:2];
    cell = [viewController tableView:nil cellForRowAtIndexPath:path];
    XCTAssertEqualObjects(cell.textLabel.text, @"eschrodinger@yahoo.com");

    path = [NSIndexPath indexPathForRow:2 inSection:2];
    cell = [viewController tableView:nil cellForRowAtIndexPath:path];
    XCTAssertEqualObjects(cell.textLabel.text, @"schrodinger@university.org");
}

- (void) prepareData
{
    ContactDao* contactDao = [[ContactDao alloc] init];
    EmailDao* emailDao = [[EmailDao alloc] init];

    Contact* contact = [contactDao insertNew];
    contact.contactID = @"1";
    contact.firstName = @"Erwin";
    contact.lastName = @"Schrödinger";
    contact.street = @"14 Millergasse";
    contact.city = @"Vienna";
    contact.zip = @"1030";
    contact.state = @"Austria";
    contact.phone = @"+43016435221";
    contact.favorite = @(NO);

    Email* mail1 = [emailDao insertNew];
    Email* mail2 = [emailDao insertNew];
    Email* mail3 = [emailDao insertNew];

    mail1.address = @"erwin.schrodinger@gmail.com";
    mail2.address = @"eschrodinger@yahoo.com";
    mail3.address = @"schrodinger@university.org";

    mail1.contact = contact;
    mail2.contact = contact;
    mail3.contact = contact;

    [contactDao save];
}

@end
