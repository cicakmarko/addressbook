//
// Created by Marko Cicak on 4/1/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "TestAppDelegate.h"


@implementation TestAppDelegate

- (BOOL) application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    NSBundle* bundle = [NSBundle bundleForClass:self.class];
    UIStoryboard* s = [UIStoryboard storyboardWithName:@"Test" bundle:bundle];
    self.window.rootViewController = [s instantiateInitialViewController];
    [self.window makeKeyAndVisible];
    return YES;
}

@end
