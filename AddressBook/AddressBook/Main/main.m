//
//  main.m
//  AddressBook
//
//  Created by Marko Cicak on 4/1/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "AppDelegate.h"

int main(int argc, char* argv[]) {
    int ret;
    @autoreleasepool
    {
        if (TESTING)
        {
            ret = UIApplicationMain(argc, argv, nil, @"TestAppDelegate");
        }
        else
        {
            ret = UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
        }

        return ret;
    }
}
