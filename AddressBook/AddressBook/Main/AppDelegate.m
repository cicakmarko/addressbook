//
//  AppDelegate.m
//  AddressBook
//
//  Created by Marko Cicak on 4/1/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "AppDelegate.h"
#import "ContactViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL) application:(UIApplication*)application willFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
#if TARGET_IPHONE_SIMULATOR
// where are you?
NSLog(@"Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
inDomains:NSUserDomainMask] lastObject]);
#endif
    return YES;
}

- (BOOL) application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
//    ContactViewController* cvc = [[ContactViewController alloc] initWithStyle:UITableViewStyleGrouped];
//    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:cvc];
//    self.window.rootViewController = nc;
//    [self.window makeKeyAndVisible];
    return YES;
}

@end
