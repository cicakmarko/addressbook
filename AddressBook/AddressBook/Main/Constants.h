//
// Created by Marko Cicak on 4/7/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#ifndef CenterDevice_Constants_h
#define CenterDevice_Constants_h

FOUNDATION_EXPORT NSString* const kCCBaseUrl;

// Cell Identifiers
FOUNDATION_EXPORT NSString* const CellIdPhone;
FOUNDATION_EXPORT NSString* const CellIdAddress;
FOUNDATION_EXPORT NSString* const CellIdEmail;

#endif
