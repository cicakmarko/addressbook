//
//  AppDelegate.h
//  AddressBook
//
//  Created by Marko Cicak on 4/1/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property(strong, nonatomic) UIWindow* window;

@end
