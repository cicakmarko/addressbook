//
//  Contact.h
//  AddressBook
//
//  Created by Marko Cicak on 4/8/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Email;

@interface Contact : NSManagedObject

@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * contactID;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * street;
@property (nonatomic, retain) NSString * zip;
@property (nonatomic, retain) NSNumber * favorite;
@property (nonatomic, retain) NSSet *emails;
@end

@interface Contact (CoreDataGeneratedAccessors)

- (void)addEmailsObject:(Email *)value;
- (void)removeEmailsObject:(Email *)value;
- (void)addEmails:(NSSet *)values;
- (void)removeEmails:(NSSet *)values;

@end
