//
//  Email.m
//  AddressBook
//
//  Created by Marko Cicak on 4/8/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "Email.h"
#import "Contact.h"


@implementation Email

@dynamic address;
@dynamic contact;

@end
