//
// Created by Marko Cicak on 3/6/13.
//
//  Copyright (c) 2013 codecentric d.o.o. All rights reserved.
//

#import "GenericDao.h"

@implementation GenericDao

- (id) init
{
    self = [super init];
    if (self)
    {
    }

    return self;
}

- (id) initWithManagedObjectContext:(NSManagedObjectContext*)moc
{
    if (self = [self init])
    {
        _managedObjectContext = moc;
    }
    return self;
}

- (NSEntityDescription*) entityDescription
{
    if (!_entityDescription)
    {
        _entityDescription = [NSEntityDescription entityForName:self.entityName
                                         inManagedObjectContext:self.managedObjectContext];
    }

    return _entityDescription;
}

- (NSFetchRequest*) fetchRequest
{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    [request setEntity:self.entityDescription];
    if (self.defaultOrderBy)
    {
        [request setSortDescriptors:@[ self.defaultOrderBy ]];
    }

    return request;
}

- (NSArray*) fetchWithPredicate:(NSPredicate*)predicate andSortDescriptors:(NSArray*)sortDescriptors
{
    return [self fetchWithPredicate:predicate sortDescriptors:sortDescriptors propertiesToFetch:nil];
}

- (NSArray*) fetchWithPredicate:(NSPredicate*)predicate
                sortDescriptors:(NSArray*)sortDescriptors
              propertiesToFetch:(NSArray*)propertiesToFetch
{
    NSFetchRequest* request = [self fetchRequest];

    if (predicate)
    {
        [request setPredicate:predicate];
    }

    if (sortDescriptors)
    {
        [request setSortDescriptors:sortDescriptors];
    }

    request.propertiesToFetch = propertiesToFetch;

    NSError* error;
    NSArray* items = [self.managedObjectContext executeFetchRequest:request error:&error];
    if (!items)
    {
        NSLog(@"Error while fetching entities: %@, %@", error, [error userInfo]);
    }

    return items;
}

- (BOOL) isTableEmpty
{
    NSError* error;
    NSUInteger count = [self.managedObjectContext countForFetchRequest:self.fetchRequest error:&error];
    if (count == NSNotFound)
    {
        NSLog(@"isTableEmpty error: %@", [error localizedDescription]);
        return YES;
    }
    return count == 0;
}

- (id) insertNew
{
    return [NSEntityDescription insertNewObjectForEntityForName:[self entityName]
                                         inManagedObjectContext:self.managedObjectContext];
}

- (id) getExistingOrNew:(id)entityID
{
    id entity = [self findByID:entityID];
    if (!entity)
    {
        entity = [self insertNew];
        [entity setValue:entityID forKey:self.attributeID];
    }
    return entity;
}

- (NSArray*) findAll
{
    if (!self.defaultOrderBy)
    {
        return [self fetchWithPredicate:nil andSortDescriptors:nil];
    }
    else
    {
        return [self fetchWithPredicate:nil andSortDescriptors:@[ self.defaultOrderBy ]];
    }
}

- (id) findByID:(id)entityID
{
    if (!self.attributeID)
    {
        [NSException raise:@"ID attribute is not defined" format:@"no ID attribute"];
    }

    NSPredicate* predicate;
    if ([entityID isKindOfClass:[NSString class]])
    {
        predicate = [NSPredicate predicateWithFormat:@"%K == %@", self.attributeID, entityID];
    }
    else if ([entityID isKindOfClass:[NSNumber class]])
    {
        NSNumber* number = entityID;
        predicate = [NSPredicate predicateWithFormat:@"%K == %d", self.attributeID, [number integerValue]];
    }
    NSArray* result = [self fetchWithPredicate:predicate andSortDescriptors:nil];
    if (result.count > 0)
    {
        return [result lastObject];
    }
    else
    {
        return nil;
    }
}

/*
 * Creates IN query and fetch existing objects from database.
 * Fetched entities are sorted by sortDescriptors parameter.
 *
 * @return NSArray of existing Core Data objects
 */
- (NSArray*) findAllByIDs:(NSArray*)entityIDs sortDescriptors:(NSArray*)sortDescriptors
{
    if (!self.attributeID)
    {
        [NSException raise:@"ID attribute is not defined" format:@"no ID attribute"];
    }

    if (!entityIDs)
    {
        return @[ ];
    }

    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(%K IN %@)", self.attributeID, entityIDs];
    NSArray* results = [self fetchWithPredicate:predicate andSortDescriptors:sortDescriptors];
    return results;
}

- (NSArray*) findAllByIDs:(NSArray*)entityIDs
          sortDescriptors:(NSArray*)sortDescriptors
        propertiesToFetch:(NSArray*)properties
{
    if (!self.attributeID)
    {
        [NSException raise:@"ID attribute is not defined" format:@"no ID attribute"];
    }

    if (!entityIDs)
    {
        return @[ ];
    }

    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(%K IN %@)", self.attributeID, entityIDs];
    NSArray* results = [self fetchWithPredicate:predicate andSortDescriptors:sortDescriptors];
    return results;
}

- (void) putDomainObject:(id)domainObject andSave:(BOOL)save
{
    if (domainObject)
    {
        NSString* stringID = [domainObject valueForKey:self.attributeID];
        id coreDataObject = [self findByID:stringID];

        if (!coreDataObject)
        {
            coreDataObject = [self insertNew];
        }

        [self domain:domainObject toCoreDataModel:coreDataObject];

        if (save)
        {
            [self save];
        }
    }
}


/*
 * This method to following steps:
 * 1) Create array of id's from entities fetched from the Rest server
 * 2) Sort the id's in ascending order
 * 3) Create IN query and fetch existing entities from database
 * 4) Iterate through fetched entities and update or delete them
 * 5) Insert new entities in core data
 *
 * @param id domainObject - Domain object, ie CDDocument
 * @param BOOL save - should Managed Object Context be saved after storing object into CoreData
 */
- (void) updateOrCreateNew:(NSArray*)domainObjects andSave:(BOOL)save
{
    if (domainObjects)
    {
        // 1) Create array of id's from objects fetched from the Rest server
        NSArray* entitiesID = [self entityIDsFromDomainObjects:domainObjects];

        // 2) Sort the id's in ascending order
        NSMutableArray* sortedEntitiesID = [NSMutableArray arrayWithArray:[self sortedArrayUsingEntityIDs:entitiesID
                                                                                          caseInsensitive:YES]];

        // 3) Create IN query and fetch existing objects from database
        NSArray* existingElements = [self entitiesByIDs:sortedEntitiesID sortAscending:YES];

        // 4) Iterate through existing elements and update them or delete them
        for (id cdElement in existingElements)
        {
            NSString* cdElementID = [cdElement valueForKeyPath:self.attributeID];

            //4a) Update element with data retrieved from the Rest server
            if ([sortedEntitiesID containsObject:cdElementID])
            {
                NSInteger domainObjectIndex = [entitiesID indexOfObject:cdElementID];
                id wsDomainObject = domainObjects[(NSUInteger) domainObjectIndex];
                [self domain:wsDomainObject toCoreDataModel:cdElement];
                [sortedEntitiesID removeObject:cdElementID];
            }
            else
            {
                // 4b)Element exists in our database, but not on the server.
                //    That means it's deleted from other client
                [self delete:cdElement andSave:NO];
            }
        }

        //5) If still exists any element in sortedAttributeIDs array, it's the new one.
        for (NSString* newEntityID in sortedEntitiesID)
        {
            @autoreleasepool
            {
                id newEntity = [self insertNew];
                NSInteger domainObjectIndex = [entitiesID indexOfObject:newEntityID];
                id wsDomainObject = domainObjects[(NSUInteger) domainObjectIndex];
                [self domain:wsDomainObject toCoreDataModel:newEntity];
            }
        }

        if (save)
        {
            [self save];
        }
    }
}


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Creates NSArray of id's from passed domain objects.
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
- (NSArray*) entityIDsFromDomainObjects:(NSArray*)domainObjects
{
    if (!domainObjects)
    {
        return @[ ];
    }

    NSMutableArray* attributeIDs = [NSMutableArray arrayWithCapacity:[domainObjects count]];

    // 1) Create array of id's from domain objects
    for (id wsDomainObject in domainObjects)
    {
        [attributeIDs addObject:[wsDomainObject valueForKeyPath:self.attributeID]];
    }
    return attributeIDs;
}

/*
 * Returns sorted array of strings using compare: or caseInsensitiveCompare: methods
 */
- (NSArray*) sortedArrayUsingEntityIDs:(NSArray*)entityIDs caseInsensitive:(BOOL)caseInsensitive
{
    if (caseInsensitive)
    {
        return [entityIDs sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    }
    return [entityIDs sortedArrayUsingSelector:@selector(compare:)];
}

/*
 * Creates IN query and fetch existing objects from database
 * Fetched entities are sorted by attributeID.
 * Order of entities depends on sortAscending parameter.
 *
 * @return NSArray of existing Core Data objects
 */
- (NSArray*) entitiesByIDs:(NSArray*)entityIDs sortAscending:(BOOL)ascending
{
    NSSortDescriptor* sortByID = [NSSortDescriptor sortDescriptorWithKey:self.attributeID ascending:ascending];
    NSArray* existingEntities = [self findAllByIDs:entityIDs sortDescriptors:@[ sortByID ]];
    return existingEntities;
}


- (BOOL) delete:(id)entity andSave:(BOOL)save
{
    [self.managedObjectContext deleteObject:entity];
    if (save)
    {
        return [self save];
    }
    else
    {
        return YES;
    }
}

- (BOOL) deleteEntities:(NSArray*)entities andSave:(BOOL)save
{
    for (NSManagedObject* entity in entities)
    {
        [self.managedObjectContext deleteObject:entity];
    }

    if (save)
    {
        return [self save];
    }
    else
    {
        return YES;
    }
}

/* Fetch 'em all and delete them all. */
- (BOOL) deleteAll
{
    NSFetchRequest* allEntities = [[NSFetchRequest alloc] init];
    allEntities.entity = [NSEntityDescription entityForName:self.entityName
                                     inManagedObjectContext:self.managedObjectContext];
    allEntities.includesPropertyValues = NO; // only include object IDs

    NSError* error = nil;
    NSArray* entities = [self.managedObjectContext executeFetchRequest:allEntities error:&error];

    for (id entity in entities)
    {
        [self.managedObjectContext deleteObject:entity];
    }

    return [self save];
}

- (BOOL) save
{
    NSError* error;
    if (![self.managedObjectContext save:&error])
    {
        NSLog(@"Error saving %@: %@", self.entityName, error);
        return NO;
    }

    return YES;
}

- (void) domain:(id)domainObject toCoreDataModel:(id)managedObject
{
    // abstract
}

- (void) coreDataModel:(id)managedObject toDomain:(id)domainObject
{
    // abstract
}

- (NSManagedObjectContext*) managedObjectContext
{
    if (!_managedObjectContext)
    {
        _managedObjectContext = SREG.coredata.managedObjectContext;
    }
    return _managedObjectContext;
}

@end
