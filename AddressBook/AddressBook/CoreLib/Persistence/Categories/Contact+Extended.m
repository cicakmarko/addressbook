//
// Created by Marko Cicak on 4/10/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "Contact+Extended.h"


@implementation Contact (Extended)

- (NSString*) lastnameFirstLetter
{
    return [[self.lastName substringToIndex:1] uppercaseString];
}

- (NSString*) fullname
{
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

@end
