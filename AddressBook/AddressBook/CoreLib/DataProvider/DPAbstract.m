//
// Created by Marko Cicak on 4/7/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "DPAbstract.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"

@implementation DPAbstract

- (void) execute
{
    self.inProgress = YES;
    [self.client GET:self.path parameters:self.params success:self.successBlock failure:self.failureBlock];
}

- (id) params
{
    return nil;
}

- (NSString*) path
{
    return nil;
}

- (SuccessBlock) successBlock
{
    return ^(AFHTTPRequestOperation* operation, id responseObject) {
        self.inProgress = NO;
    };
}

- (FailureBlock) failureBlock
{
    return ^(AFHTTPRequestOperation* operation, NSError* error) {
        self.inProgress = NO;
    };
}

@end
