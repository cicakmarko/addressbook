//
// Created by Marko Cicak on 4/7/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "DPAllContacts.h"
#import "ContactDao.h"
#import "Contact.h"


@implementation DPAllContacts

- (NSString*) path
{
    return @"contacts";
}

- (SuccessBlock) successBlock
{
    return ^(AFHTTPRequestOperation* operation, id o) {
        NSArray* contacts = o;
        ContactDao* contactDao = [[ContactDao alloc] init];
        for (NSDictionary* contact in contacts)
        {
            NSString* contactID = contact[@"id"];
            Contact* dbContact = [contactDao getExistingOrNew:contactID];
            dbContact.contactID = contactID;
            dbContact.firstName = contact[@"firstname"];
            dbContact.lastName = contact[@"lastname"];
        }
        [contactDao save];
        self.inProgress = NO;
    };
}

- (FailureBlock) failureBlock
{
    return ^(AFHTTPRequestOperation* operation, NSError* error) {
    };
}

@end
