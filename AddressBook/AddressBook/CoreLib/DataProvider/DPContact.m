//
// Created by Marko Cicak on 4/20/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "DPContact.h"
#import "ContactDao.h"
#import "Contact.h"
#import "EmailDao.h"
#import "Email.h"

@interface DPContact ()
@property(nonatomic, copy) NSString* contactID;
@end

@implementation DPContact

- (instancetype) initWithContactID:(NSString*)contactID
{
    if ((self = [super init]))
    {
        _contactID = contactID;
    }
    return self;
}

- (NSString*) path
{
    return [NSString stringWithFormat:@"contact/%@", self.contactID];
}

- (SuccessBlock) successBlock
{
    return ^(AFHTTPRequestOperation* operation, id o) {
        ContactDao* contactDao = [[ContactDao alloc] init];
        EmailDao* emailDao = [[EmailDao alloc] init];

        NSDictionary* response = o;
        Contact* contact = [contactDao findByID:self.contactID];
        contact.firstName = response[@"firstname"];
        contact.lastName = response[@"lastname"];
        contact.phone = response[@"phone"];
        contact.street = response[@"street"];
        contact.zip = response[@"zip"];
        contact.city = response[@"city"];
        contact.state = response[@"state"];

        [emailDao deleteEntities:contact.emails.allObjects andSave:NO];
        NSArray* emails = response[@"emails"];
        for (NSString* emailAddress in emails)
        {
            Email* email = [emailDao insertNew];
            email.address = emailAddress;
            [contact addEmailsObject:email];
        }

        [contactDao save];
        NSDictionary* info = @{ @"contactID" : self.contactID };
        [[NSNotificationCenter defaultCenter]
                postNotificationName:@"ContactRetrievedNotification" object:nil userInfo:info];
        self.inProgress = NO;
    };
}

- (FailureBlock) failureBlock
{
    return [super failureBlock];
}

@end
