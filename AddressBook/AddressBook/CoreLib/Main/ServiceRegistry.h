//
// Created by Marko Cicak on 4/1/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

@class CoreDataStack;
@class Context;
@class AFHTTPRequestOperationManager;
@protocol DataProvider;

@interface ServiceRegistry : NSObject

@property(nonatomic, strong) CoreDataStack* coredata;
@property(nonatomic, strong) Context* context;
@property(nonatomic, strong) id <DataProvider> dataProvider;

+ (ServiceRegistry*) instance;

@end
