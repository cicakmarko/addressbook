//
// Created by Marko Cicak on 4/1/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Context : NSObject

- (NSURL*) applicationDocumentsDirectoryURL;

@end
