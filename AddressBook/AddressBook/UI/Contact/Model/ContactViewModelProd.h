//
// Created by Marko Cicak on 4/18/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactViewModel.h"


@interface ContactViewModelProd : NSObject <ContactViewModel>
@property(nonatomic, copy) NSString* contactID;

- (instancetype) initWithContactID:(NSString*)contactID;
@end
