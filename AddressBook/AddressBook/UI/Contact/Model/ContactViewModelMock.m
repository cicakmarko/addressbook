//
// Created by Marko Cicak on 4/18/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "ContactViewModelMock.h"

@implementation ContactViewModelMock

- (NSInteger) emailsCount
{
    return 3;
}

- (NSString*) fullname
{
    return @"Erwin Schrödinger";
}

- (NSString*) phone
{
    return @"+43016435221";
}

- (NSString*) street
{
    return @"14 Millergasse";
}

- (NSString*) state
{
    return @"Austria";
}

- (NSString*) city
{
    return @"1030 Vienna";
}

- (NSString*) emailAtIndex:(NSInteger)idx
{
    return @[
            @"erwin.schrodinger@gmail.com",
            @"eschrodinger@yahoo.com",
            @"schrodinger@university.org"
    ][(NSUInteger) idx];
}

@end
