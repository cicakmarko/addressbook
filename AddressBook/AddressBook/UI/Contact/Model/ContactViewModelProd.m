//
// Created by Marko Cicak on 4/18/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "ContactViewModelProd.h"
#import "ContactDao.h"
#import "Contact.h"
#import "Contact+Extended.h"
#import "Email.h"

@interface ContactViewModelProd ()
@property(nonatomic, strong) ContactDao* contactDao;
@property(nonatomic, strong) Contact* contact;
@end


@implementation ContactViewModelProd

- (instancetype) initWithContactID:(NSString*)contactID
{
    if ((self = [super init]))
    {
        _contactID = contactID;
    }
    return self;
}

- (NSInteger) emailsCount
{
    return self.contact.emails.count;
}

- (NSString*) fullname
{
    return self.contact.fullname;
}

- (NSString*) phone
{
    return self.contact.phone;
}

- (NSString*) street
{
    return self.contact.street;
}

- (NSString*) city
{
    return [NSString stringWithFormat:@"%@ %@", self.contact.zip, self.contact.city];
}

- (NSString*) state
{
    return self.contact.state;
}

- (NSString*) emailAtIndex:(NSInteger)idx
{
    NSArray* emails = self.contact.emails.allObjects;
    emails = [emails sortedArrayUsingDescriptors:@[
            [NSSortDescriptor sortDescriptorWithKey:@"address" ascending:YES] ]];
    return [emails[(NSUInteger) idx] address];
}

#pragma mark - Getters

- (ContactDao*) contactDao
{
    if (!_contactDao)
    {
        _contactDao = [[ContactDao alloc] init];
    }
    return _contactDao;
}

- (Contact*) contact
{
    if (!_contact)
    {
        _contact = [self.contactDao findByID:self.contactID];
    }
    return _contact;
}

@end
