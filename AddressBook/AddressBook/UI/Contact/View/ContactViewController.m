//
// Created by Marko Cicak on 4/13/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "ContactViewController.h"
#import "ContactViewModel.h"
#import "ContactViewModelMock.h"
#import "Contact.h"
#import "ContactHeaderView.h"
#import "ContactAddressCell.h"
#import "ContactViewModelProd.h"
#import "DataProvider.h"

#define SECTION_PHONE   0
#define SECTION_ADDRESS 1
#define SECTION_EMAILS  2

@interface ContactViewController ()
@property(nonatomic, strong) id <ContactViewModel> model;
@property(nonatomic, strong) ContactHeaderView* tableHeaderView;
@end


@implementation ContactViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
//            initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(didTapEditButton:)];

    self.tableView.tableHeaderView = self.tableHeaderView;
    [self.tableView registerClass:UITableViewCell.class forCellReuseIdentifier:@"EmailCell"];
    [self.tableView registerClass:ContactAddressCell.class forCellReuseIdentifier:@"AddressCell"];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [SREG.dataProvider retrieveContactWithID:self.contactID];
    [[NSNotificationCenter defaultCenter]
            addObserver:self selector:@selector(didRetrieveContact:) name:@"ContactRetrievedNotification" object:nil];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

#pragma mark - UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView*)tableView
{
    return 3;
}

- (NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section)
    {
        case SECTION_PHONE:
            return 1;
        case SECTION_ADDRESS:
            return 1;
        case SECTION_EMAILS:
        default:
            return self.model.emailsCount;
    }
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    switch (indexPath.section)
    {
        case SECTION_PHONE:
            return [self cellPhone];
        case SECTION_ADDRESS:
            return [self cellAddressForRow:indexPath.row];
        case SECTION_EMAILS:
        default:
            return [self cellEmailForRow:indexPath.row];
    }
}

- (UITableViewCell*) cellPhone
{
    UITableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdPhone];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdPhone];
    }
    cell.textLabel.text = self.model.phone;
    return cell;
}

- (UITableViewCell*) cellAddressForRow:(NSInteger)row
{
    NSIndexPath* path = [NSIndexPath indexPathForRow:row inSection:SECTION_ADDRESS];
    ContactAddressCell* cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdAddress forIndexPath:path];
    cell.streetLabel.text = self.model.street;
    cell.cityLabel.text = self.model.city;
    cell.stateLabel.text = self.model.state;
    return cell;
}

- (UITableViewCell*) cellEmailForRow:(NSInteger)row
{
    static NSString* CellEmailId = @"EmailCell";
    NSIndexPath* path = [NSIndexPath indexPathForRow:row inSection:SECTION_EMAILS];
    UITableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:CellEmailId forIndexPath:path];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellEmailId];
    }
    cell.textLabel.text = [self.model emailAtIndex:row];
    return cell;
}

- (NSString*) tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section)
    {
        case SECTION_PHONE:
            return @"Phone";
        case SECTION_ADDRESS:
            return @"Address";
        case SECTION_EMAILS:
        default:
            return @"Emails";
    }
}

- (CGFloat) tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (indexPath.section == SECTION_ADDRESS)
    {
        return 98;
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

#pragma mark - Actions

- (void) didTapEditButton:(id)didTapEditButton
{
    NSLog(@"EDIT");
}

#pragma mark - Notifications

- (void) didRetrieveContact:(NSNotification*)notif
{
    NSString* contactID = notif.userInfo[@"contactID"];
    if (![self.contactID isEqualToString:contactID])
    {
        return;
    }

    [self.tableView reloadData];
}

#pragma mark - Getters

- (id <ContactViewModel>) model
{
    if (!_model)
    {
//        _model = [[ContactViewModelMock alloc] init];
        _model = [[ContactViewModelProd alloc] initWithContactID:self.contactID];
    }
    return _model;
}

- (ContactHeaderView*) tableHeaderView
{
    if (!_tableHeaderView)
    {
        _tableHeaderView = [[ContactHeaderView alloc] initWithFrame:CGRectMake(0, 0, 0, 44)];
        _tableHeaderView.titleLabel.text = self.model.fullname;
    }
    return _tableHeaderView;
}

@end
