//
// Created by Marko Cicak on 4/20/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "FavoriteViewModelProd.h"
#import "ContactDao.h"
#import "UITableViewCell+Contact.h"
#import "Contact.h"

@interface FavoriteViewModelProd ()
@property(nonatomic, strong) NSFetchedResultsController* fetchedResultsController;
@property(nonatomic, strong) ContactDao* contactDao;
@end


@implementation FavoriteViewModelProd

- (void) tableView:(UITableView*)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
 forRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        Contact* contact = [self contactAtIndex:indexPath.row];
        contact.favorite = @(NO);
        [contact.managedObjectContext save:nil];
    }
}

- (NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[(NSUInteger) section];
    return sectionInfo.numberOfObjects;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"FavoriteCell" forIndexPath:indexPath];
    Contact* contact = [self contactAtIndex:indexPath.row];
    [cell configureWithFavoriteContact:contact];
    return cell;
}

- (Contact*) contactAtIndex:(NSInteger)idx
{
    NSIndexPath* path = [NSIndexPath indexPathForRow:idx inSection:0];
    return [self.fetchedResultsController objectAtIndexPath:path];
}

- (void) setFetchedResultsControllerDelegate:(id <NSFetchedResultsControllerDelegate>)delegate
{
    self.fetchedResultsController.delegate = delegate;
}

#pragma mark - Getters

- (NSFetchedResultsController*) fetchedResultsController
{
    if (!_fetchedResultsController)
    {
        NSFetchRequest* request = self.contactDao.requestForFavoriteContacts;
        request.sortDescriptors = @[
                [NSSortDescriptor sortDescriptorWithKey:@"lastName" ascending:YES selector:@selector(caseInsensitiveCompare:)],
                [NSSortDescriptor sortDescriptorWithKey:@"firstName" ascending:YES selector:@selector(caseInsensitiveCompare:)]
        ];

        [request setFetchBatchSize:50];

        NSFetchedResultsController* aFetchedResultsController = [[NSFetchedResultsController alloc]
                initWithFetchRequest:request
                managedObjectContext:SREG.coredata.managedObjectContext
                  sectionNameKeyPath:nil
                           cacheName:nil];
        _fetchedResultsController = aFetchedResultsController;

        NSError* error;
        if (![_fetchedResultsController performFetch:&error])
        {
            NSLog(@"ERROR: %@", error.localizedDescription);
            abort();
        }
    }
    return _fetchedResultsController;
}

- (ContactDao*) contactDao
{
    if (!_contactDao)
    {
        _contactDao = [[ContactDao alloc] init];
    }
    return _contactDao;
}

@end
