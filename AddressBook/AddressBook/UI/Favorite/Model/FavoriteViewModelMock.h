//
// Created by Marko Cicak on 4/20/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FavoriteViewModel.h"


@interface FavoriteViewModelMock : NSObject <FavoriteViewModel>
@end
