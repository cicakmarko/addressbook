//
// Created by Marko Cicak on 4/20/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Contact;

@protocol FavoriteViewModel <UITableViewDataSource>
- (Contact*) contactAtIndex:(NSInteger)idx;

- (void) setFetchedResultsControllerDelegate:(id <NSFetchedResultsControllerDelegate>)delegate;
@end
