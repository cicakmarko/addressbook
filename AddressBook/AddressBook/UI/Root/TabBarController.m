//
//  TabBarController.m
//  AddressBook
//
//  Created by Marko Cicak on 4/1/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "TabBarController.h"

@interface TabBarController ()
@end

@implementation TabBarController

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.selectedIndex = 1;
}

@end
