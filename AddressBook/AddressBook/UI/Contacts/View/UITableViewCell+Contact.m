//
// Created by Marko Cicak on 4/8/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "UITableViewCell+Contact.h"
#import "Contact.h"


@implementation UITableViewCell (Contact)

- (void) configureWithContact:(Contact*)contact
{
    UIFont* regularFont = [UIFont systemFontOfSize:self.textLabel.font.pointSize];
    UIFont* boldFont = [UIFont boldSystemFontOfSize:self.textLabel.font.pointSize];

    NSDictionary* firstNameAttributes = @{ NSFontAttributeName : regularFont,
                                           NSStrokeColorAttributeName : [UIColor blackColor] };
    NSDictionary* lastNameAttributes = @{ NSFontAttributeName : boldFont,
                                          NSStrokeColorAttributeName : [UIColor blackColor] };

    NSMutableAttributedString* name = [[NSMutableAttributedString alloc]
            initWithString:contact.firstName attributes:firstNameAttributes];
    NSString* last = [NSString stringWithFormat:@" %@", contact.lastName];
    NSMutableAttributedString* lastName = [[NSMutableAttributedString alloc]
            initWithString:last attributes:lastNameAttributes];
    [name appendAttributedString:lastName];

    [self.textLabel setAttributedText:name];
}

- (void) configureWithFavoriteContact:(Contact*)contact
{
    NSString* fullname = [NSString stringWithFormat:@"%@ %@", contact.firstName, contact.lastName];
    self.textLabel.text = fullname;
    self.detailTextLabel.font = [UIFont systemFontOfSize:13];
    self.detailTextLabel.text = contact.city;
}

@end
