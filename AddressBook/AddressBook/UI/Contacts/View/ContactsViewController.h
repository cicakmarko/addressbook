//
// Created by Marko Cicak on 4/2/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ContactsViewController : UITableViewController
@property(nonatomic, assign) BOOL pickingMode;
@end
