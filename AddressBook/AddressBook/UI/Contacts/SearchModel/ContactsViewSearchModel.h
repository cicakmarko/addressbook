//
// Created by Marko Cicak on 4/10/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Contact;


@interface ContactsViewSearchModel : NSObject <UITableViewDataSource>

@property(nonatomic, strong) NSString* query;

- (Contact*) contactAtIndexPath:(NSIndexPath*)path;

@end
